package tn.esprit.jpa;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Tache implements Serializable {

	private int dure;
	
	@EmbeddedId
	private TachePk pk;

	public int getDure() {
		return dure;
	}

	public void setDure(int dure) {
		this.dure = dure;
	}

	public TachePk getPk() {
		return pk;
	}

	public void setPk(TachePk pk) {
		this.pk = pk;
	}
	
	@ManyToOne
	@JoinColumn(referencedColumnName="idProj",name="idProjet",
				insertable=false,updatable=false)
	private Projet projet;
	
	@ManyToOne
	@JoinColumn(referencedColumnName="idEmploye",name="idEmp",
				insertable=false,updatable=false)
	private Employe employe;

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	
	
	
}
