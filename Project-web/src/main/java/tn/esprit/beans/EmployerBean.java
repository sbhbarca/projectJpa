package tn.esprit.beans;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import tn.esprit.ejb.IEmployerLocal;
import tn.esprit.jpa.Employe;
import tn.esprit.jpa.Nationalite;

@ManagedBean
@ViewScoped
public class EmployerBean {
	
	@EJB
	IEmployerLocal employerLocal;
	// update employer
	private Employe emp;
	// add employer
	private Employe employer;
	private List<Employe> employes;
	private String name;
	private String lastname;
	private String success;
	private Nationalite[] nationalites;
	private boolean etat;
	
	@PostConstruct
	public void init(){
		etat = false;
		employer = new Employe();
		setNationalites(Nationalite.values());
		setEmployes(employerLocal.findAllEmploye());
	}
	
	public void addEmp(){
		employerLocal.addEmp(employer);
		success="Employer Ajouter avec succ�s";
		employer = new Employe();
	}
	
	public void preUpdateEmpl(Employe e){
		etat =true;
		emp = employerLocal.findById(e.getIdEmploye());
		emp.setName(e.getName());
		emp.setLastname(e.getLastname());
		emp.setNationalite(e.getNationalite());
		emp.setEmail(e.getEmail());
	}
	
	public void updateEmp(){
		employerLocal.updateEmp(emp);
		setEmployes(employerLocal.findAllEmploye());
		emp= new Employe();
		etat = false;
	}
	
	public void deleteEmpl(Integer id){
		employerLocal.deleteEmp(id);
		setEmployes(employerLocal.findAllEmploye());
	}
	
	public Employe getEmployer() {
		return employer;
	}
	public void setEmployer(Employe employer) {
		this.employer = employer;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public List<Employe> getEmployes() {
		return employes;
	}

	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public Nationalite[] getNationalites() {
		return nationalites;
	}

	public void setNationalites(Nationalite[] nationalites) {
		this.nationalites = nationalites;
	}

	public Employe getEmp() {
		return emp;
	}

	public void setEmp(Employe emp) {
		this.emp = emp;
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	
	
}
